<?php

namespace app\mall\controller\order;

use app\mall\controller\Controller;
use app\mall\model\order\Order as OrderModel;
use app\mall\model\order\OrderRefund as OrderRefundModel;
use app\mall\model\setting\ReturnAddress as ReturnAddressModel;

/**
 * 平台售后管理
 */
class PlateRefund extends Controller
{
    /**
     * 售后列表
     */
    public function index()
    {
        $model = new OrderRefundModel;
        $params = $this->postData();
        //列表数据
        $list = $model->getplateList($params);
        //重要数字
        $num_arr = $model->plategroupCount($params);
        $arr = [];
        $arr[40] = $model->finishcount();
        foreach ($num_arr as $key => $val) {
            $k = $val['plate_status']['value'];
            $arr[$k] = $val;
        }

        return $this->renderSuccess('', compact('list', 'arr'));
    }

    /**
     * 售后单详情
     */
    public function detail($order_refund_id)
    {
        // 售后单详情
        $detail = OrderRefundModel::detail($order_refund_id);
        if (isset($detail['send_time']) && $detail['send_time'] > 0) {
            $detail['send_time'] = date('Y-m-d H:i:s', $detail['send_time']);
        }
        // 订单详情
        $order = OrderModel::detail($detail['order_id']);
        // 退货地址
        $address = (new ReturnAddressModel)->getAll($detail['shop_supplier_id']);
        return $this->renderSuccess('', compact('detail', 'order', 'address'));
    }

    /**
     * 商家审核
     */
    public function audit($order_refund_id)
    {
        $model = OrderRefundModel::detail($order_refund_id);
        if ($model->plateaudit($this->postData())) {
            return $this->renderSuccess('操作成功');
        }
        return $this->renderError($model->getError() ?: '操作失败');
    }

}